# OAuth2.0 

**Introduction**

-   [OAuth 2.0](https://tools.ietf.org/html/rfc6749), which stands for
    "Open Authorization", is a standard designed to allow a website or
    application to access resources hosted by other web apps on behalf
    of a user

-   OAuth 2.0 is an authorization protocol and NOT an authentication
    protocol.

-   <ins>OAuth Roles:</ins>

    -   <ins>Resource Owner (User)</ins>: The user or system that
        owns the protected resources and can grant access to them. *(in
        our case, the resource owner is an employer who uses MERP)*

    -   <ins>Client (Application)</ins>: The client is the system
        that requires access to the protected resources *(in our case,
        the resource is employer identification).* To access resources,
        the Client must hold the appropriate Access Token. *(in our
        case, the client is any system in MERP)*

    -   <ins>Authorization Server </ins>: This server receives
        requests from the Client for Access Tokens and issues them upon
        successful authentication and consent by the Resource Owner. The
        authorization server exposes two endpoints: the Authorization
        endpoint, which handles the interactive authentication and
        consent of the user, and the Token endpoint, which is involved
        in a machine-to-machine interaction. *(in our case, it is the
        same of API GW)*

    -   <ins>Resource Server</ins>: A server that protects the user's
        resources and receives access requests from the Client. It
        accepts and validates an Access Token from the Client and
        returns the appropriate resources to it. *(in our case, it is
        API GW too)*

<https://oauth.net/2/>

<https://auth0.com/intro-to-iam/what-is-oauth-2> 

<https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2>


**OAuth and Laravel**

We use Passport authentication library

<https://laravel.com/docs/10.x/passport>

Main tables are:

-   <ins>Oauth_clients table </ins>: stores information about the
    OAuth clients that request access to the API, clients can be
    applications or services that want to access resources on behalf of
    a user*. (in our case, it is a subsystem of MERP)*

-   <ins>Oauth_auth_codes table </ins>: stores auth. codes generated
    during the OAuth authorization flow.

-   <ins>Oauth_access_tokens table </ins>: stores issued access tokens
    because of successful authentication and authorization. Clients to
    access protected resources on behalf of a user use access tokens.

-   <ins>Oauth_refresh_tokens table </ins>: stores refresh tokens that
    issued to clients when they request access tokens. Refresh tokens
    used to obtain a new access token without requiring the user to
    re-enter their credentials.

**<ins>How to add new Client</ins>**

1.  Define the client info on OAuth server (service name and redirect
    URL)

2.  As a result of the step above, OAuth server will give client id and
    secret to the client (must be kept secure)

    a.  Client ID: A unique identifier for your OAuth client on OAuth
        server.

    b.  Client Secret: A confidential key used to authenticate OAuth
        client when interacting with the OAuth server.

**The workflow**

1.  Redirect the user from OAuth client to OAuth server in order to
    authorize it

    a.  client_id (that was created already by asking the Admin Auth server)

    b.  callback (the redirect URL that was provided)

    c.  oauth2_host (host of Auth server)


2.  OAuth server will authenticate the user and authorize hem back to
    the OAuth client with a code.

3.  OAuth client receives the code and validate status matching (which
    has set on request on 1)

4.  OAuth client post a request to OAuth server in order to get OAuth
    server Auth token

OAuth client gets OAuth token from OAuth Server

5.  OAuth client stores OAuth token, in order to allow communicating
    between OAuth client and OAuth server

6.  OAuth client asks for the user info {name, and email} from OAuth
    server (using OAuth token)



# Gateway 

In general, API gateway is an entry point for clients to access backend
services, or APIs. It acts as a single point of entry, abstracting the
underlying architecture and providing a unified interface for clients.
API gateways offer various functionalities like request routing,
protocol translation, authentication, and rate limiting. They enable
organizations to manage and secure their APIs effectively.

For out API Gateway, it offers request routing, authentication
middleware, OAuth2 services, and rate limiting functions

**TODO:** API Gateway will provide traffic and security monitoring and
interception

**<ins>How to define a new Microservice within the system?</ins>**

1.  The microservice APIs must have a unique prefix in their endpoints
    base URL.

2.  Add microservice base URL as a configuration variable for the
    service in env.

3.  Add new condition in handleRequest function in Gateway Controller
    that contain what is the URL of service in order to check incoming
    HTTP requests and redirect the required traffic to the microservice.

4.  Define callbacks APIs if needed (the communication middleware
    between microservices)

**<ins>How to work gateway?</ins>**

In Gateway Controller, the function handleRequest takes the incoming
HTTP request object as a parameter and processes it based on the its
request as the following:

1.  Extracts the path of the incoming request using \$request-\>path().

2.  Checks if the path contains any of available microservices paths, if
    it does, then it sends the request to a required microservice URL
    (defined in the environment) then returns the response received from
    the microservice.

3.  If the path does not match any prefixes, it returns an error
    response with a status code of 404 (Not Found) and a message
    \'Invalid endpoint\'.

This function (handleRequest) acts as a middleware that intercepts
incoming requests, checks the path, and forwards the request to
different microservice based on the path. It then returns the response
received from the respective service or an error response if the path
does not match any predefined endpoints.

**TODO:** log all incoming requests, with fail or success.

**<ins>Current Microservices (HRMS System)</ins>**

1)  Recruitment Microservice:

    a.  Manage recruitment functions in HRMS system

    b.  Currently it has no callbacks for internal microservices
        communication
