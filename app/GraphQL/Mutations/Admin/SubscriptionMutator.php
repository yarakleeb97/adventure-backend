<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Subscription;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SubscriptionMutator
{
    use ApiResponser;

    public function updateEnable($rootValue, array $args, GraphQLContext $context)
    {
        $subscription = Subscription::findOrFail($args['id']);
        if($subscription->enable){
            $subscription->enable = false;
        }else{
            $subscription->enable = true;
        }
                $subscription->save();
        return $this->success($subscription, 'Updated Successfully');
    }
}
