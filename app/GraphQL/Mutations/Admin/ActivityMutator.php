<?php

namespace App\GraphQL\Mutations\Admin;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Activity;
use App\Models\ActivityInterest;
use App\Models\Interest;
use App\Models\Organiser;
use Exception;
use Illuminate\Support\Facades\DB;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\File;
final class ActivityMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        Organiser::findOrFail($args['organiser']);
        if (!in_array($args['type'], Constant::TYPE)) {
            return $this->error(null, 'Type does not exists');
        }
        if (!in_array($args['age'], Constant::AGE_LEVELS)) {
            return $this->error(null, 'Age Level does not exists');
        }
        DB::beginTransaction();
        try {
            $activity = new Activity();
            $activity->title = $args['title'];
            $activity->type = $args['type'];
            $activity->description = $args['description'];
            $file = $args['cover'];
            $path = $file->storeAs('uploads', $file->getClientOriginalName());
            $activity->cover = $path;
            $activity->region_id = $args['region'];
            $activity->address = $args['address'];
            $activity->cost = $args['cost'];
            $activity->age = $args['age'];
            $activity->start_date = $args['start_date'];
            $activity->end_date = $args['end_date'];
            $activity->organiser_id = $args['organiser'];
            $activity->save();
            for ($i = 0; $i < sizeof($args['interests']); $i++) {
                Interest::findOrFail($args['interests'][$i]);
                $this->createActivityInterestInstance($args['interests'][$i], $activity->id);
            }
            DB::commit();
            return $this->success($activity, 'Created Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error(null, 'Something went wrong');
        }
    }

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $activity = Activity::findOrFail($args['id']);
        if (isset($args['type'])) {
            if (!in_array($args['type'], Constant::TYPE)) {
                return $this->error(null, 'Type does not exists');
            }
        }
        if (isset($args['age'])) {
            if (!in_array($args['age'], Constant::AGE_LEVELS)) {
                return $this->error(null, 'Age Level does not exists');
            }
        }
        $activity->type = $args['type'] ?? $activity->type;
        $activity->title = $args['title'] ?? $activity->title;
        $activity->address = $args['address'] ?? $activity->address;
        $activity->description = $args['description'] ?? $activity->description;
        $activity->region_id = $args['region'] ?? $activity->region;
        $activity->cost = $args['cost'] ?? $activity->cost;
        $activity->age = $args['age'] ?? $activity->age;
        $activity->start_date = $args['start_date'] ?? $activity->start_date;
        $activity->end_date = $args['end_date'] ?? $activity->end_date;
        if (isset($args['organiser'])) {
            Organiser::findOrFail($args['organiser']);
            $activity->organiser_id = $args['organiser'] ?? $activity->organiser;
        }
        $activity->save();
        return $this->success($activity, 'Update Successfully');
    }

    public function updateCover($rootValue, array $args, GraphQLContext $context)
    {
        $activity = Activity::findOrFail($args['id']);
        if (File::exists(public_path($activity->cover))) {
            File::delete(public_path($activity->cover));
        }
        $file = $args['cover'];
        $path = $file->storeAs('uploads', $file->getClientOriginalName());
        $activity->cover = $path;
        $activity->save();
        return $this->success($activity, 'Update cover Successfully');
    }
    public function addInterestToActivity($rootValue, array $args, GraphQLContext $context)
    {
        $activity = Activity::findOrFail($args['id']);
        $interest = Interest::findOrFail($args['interest_id']);
        $this->createActivityInterestInstance($interest->id, $activity->id);
        return  $this->success($activity, 'Added Successfully');
    }

    public function removeInterestfromActivity($rootValue, array $args, GraphQLContext $context)
    {
        Activity::findOrFail($args['id']);
        Interest::findOrFail($args['interest_id']);
        if ($activity_interest = ActivityInterest::where('interest_id', $args['interest_id'])
            ->where('activity_id', $args['id'])->first()
        ) {
            $activity_interest->delete();
        }
        return  $this->success(null, 'Removed Successfully');
    }

    public function delete($rootValue, array $args, GraphQLContext $context)
    {
        $activity = Activity::findOrFail($args['id']);
        $activity->delete();
        return  $this->success(null, 'Deleted Successfully');
    }

    public function restore($rootValue, array $args, GraphQLContext $context)
    {
        Activity::onlyTrashed()->findOrFail($args['id'])->restore();
        return $this->success(Activity::findOrFail($args['id']), 'Restored Successfully');
    }

    private function createActivityInterestInstance($interest_id, $activity_id)
    {
        $activity_interest = new ActivityInterest();
        $activity_interest->interest_id = $interest_id;
        $activity_interest->activity_id = $activity_id;
        $activity_interest->save();
    }
}
