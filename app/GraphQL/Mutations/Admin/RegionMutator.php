<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Region;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class RegionMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $region = new Region();
        $region->name = $args['name'];
        $region->save();
        return $this->success($region, 'Created Successfully');
    }

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $region = Region::findOrFail($args['id']);
        $region->name = $args['name'];
        $region->save();
        return $this->success($region, 'Update Successfully');
    }

    public function delete($rootValue, array $args, GraphQLContext $context)
    {
        $interest = Region::findOrFail($args['id']);
        $interest->delete();
        return  $this->success(null, 'Deleted Successfully');
    }

    public function restore($rootValue, array $args, GraphQLContext $context)
    {
        Region::onlyTrashed()->findOrFail($args['id'])->restore();
        return $this->success(Region::findOrFail($args['id']), 'Restored Successfully');
    }
}
