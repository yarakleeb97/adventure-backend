<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Organiser;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\File;
final class OrganiserMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $organiser = new Organiser();
        $organiser->name = $args['name'];
        $organiser->description = $args['description'];
        $file = $args['logo'];
        $path = $file->storeAs('uploads', $file->getClientOriginalName());
        $organiser->logo = $path;
        $organiser->address = $args['address'];
        $organiser->phone = $args['phone'];
        $organiser->email = $args['email'];
        $organiser->save();
        return $this->success($organiser, 'Created Successfully');
    }

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $organiser = Organiser::findOrFail($args['id']);
        $organiser->name = $args['name'] ?? $organiser->name;
        $organiser->description = $args['description'] ?? $organiser->description;
        $organiser->address = $args['address'] ?? $organiser->address;
        $organiser->phone = $args['phone'] ?? $organiser->phone;
        $organiser->email = $args['email'] ?? $organiser->email;
        $organiser->save();
        return $this->success($organiser, 'Update Successfully');
    }

    public function updateLogo($rootValue, array $args, GraphQLContext $context)
    {
        $organiser = Organiser::findOrFail($args['id']);
        if (File::exists(public_path($organiser->logo))) {
            File::delete(public_path($organiser->logo));
        }
        $file = $args['logo'];
        $path = $file->storeAs('uploads', $file->getClientOriginalName());
        $organiser->logo = $path;
        $organiser->save();
        return $this->success($organiser, 'Update logo Successfully');
    }

    public function delete($rootValue, array $args, GraphQLContext $context)
    {
        $organiser = Organiser::findOrFail($args['id']);
        $organiser->delete();
        return  $this->success(null, 'Deleted Successfully');
    }

    public function restore($rootValue, array $args, GraphQLContext $context)
    {
        Organiser::onlyTrashed()->findOrFail($args['id'])->restore();
        return $this->success(Organiser::findOrFail($args['id']), 'Restored Successfully');
    }
}
