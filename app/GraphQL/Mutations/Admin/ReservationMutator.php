<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Reservation;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class ReservationMutator
{
    use ApiResponser;

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $reservation = Reservation::findOrFail($args['id']);
        $reservation->pay = $args['pay'];
        $reservation->coming = $args['coming'];
        $reservation->save();
        return $this->success($reservation, 'Updated Successfully');
    }
}
