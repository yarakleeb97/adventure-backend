<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Slider;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\File;
final class SliderMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $slider = new Slider();
        $slider->title = $args['title'];
        $slider->desc = $args['desc'];
        $file = $args['image'];
        $path = $file->storeAs('uploads', $file->getClientOriginalName());
        $slider->image = $path;
        $slider->save();
        return $this->success($slider, 'Created Successfully');
    }

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $slider = Slider::findOrFail($args['id']);
        $slider->title = $args['title'];
        $slider->desc = $args['desc'];
        $slider->save();
        return $this->success($slider, 'Update Successfully');
    }

    public function updateSliderImage($rootValue, array $args, GraphQLContext $context)
    {
        $slider = Slider::findOrFail($args['id']);
        if (File::exists(public_path($slider->image))) {
            File::delete(public_path($slider->image));
        }
        $file = $args['image'];
        $path = $file->storeAs('uploads', $file->getClientOriginalName());
        $slider->image = $path;
        $slider->save();
        return $this->success($slider, 'Update Successfully');
    }

    public function delete($rootValue, array $args, GraphQLContext $context)
    {
        $slider = Slider::findOrFail($args['id']);
        if (File::exists(public_path($slider->image))) {
            File::delete(public_path($slider->image));
        }
        $slider->delete();
        return  $this->success(null, 'Deleted Successfully');
    }
}
