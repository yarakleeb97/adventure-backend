<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Setting;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SettingMutator
{
    use ApiResponser;

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $setting = Setting::findOrFail($args['id']);
        $setting->value = $args['value'];
        $setting->save();
        return $this->success($setting, 'Update Successfully');
    }
}
