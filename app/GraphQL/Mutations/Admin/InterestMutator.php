<?php

namespace App\GraphQL\Mutations\Admin;

use App\Http\Traits\ApiResponser;
use App\Models\Interest;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class InterestMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $interest = new Interest();
        $interest->name = $args['name'];
        $interest->save();
        return $this->success($interest, 'Created Successfully');
    }

    public function update($rootValue, array $args, GraphQLContext $context)
    {
        $interest = Interest::findOrFail($args['id']);
        $interest->name = $args['name'];
        $interest->save();
        return $this->success($interest, 'Update Successfully');
    }

    public function delete($rootValue, array $args, GraphQLContext $context)
    {
        $interest = Interest::findOrFail($args['id']);
        $interest->delete();
        return  $this->success(null, 'Deleted Successfully');
    }

    public function restore($rootValue, array $args, GraphQLContext $context)
    {
        Interest::onlyTrashed()->findOrFail($args['id'])->restore();
        return $this->success(Interest::findOrFail($args['id']), 'Restored Successfully');
    }
}
