<?php

namespace App\GraphQL\Mutations;

use App\Http\Traits\ApiResponser;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\File;

final class FileManagementMutator
{
    use ApiResponser;

    public function upload($rootValue, array $args, GraphQLContext $context)
    {
        $file = $args['file'];
        $path = $file->storeAs('uploads', $file->getClientOriginalName());
        return $this->success(['path' => $path], 'Uploaded Successfully');
    }

    public function remove($rootValue, array $args, GraphQLContext $context)
    {
        if (File::exists(public_path($args['path']))) {
            File::delete(public_path($args['path']));
            return $this->success(null, 'Removed Successfully');
        } else {
            return $this->error(null, 'File does not exists');
        }
    }
}
