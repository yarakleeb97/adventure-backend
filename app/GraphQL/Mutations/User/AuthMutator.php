<?php

namespace App\GraphQL\Mutations\User;

use App\Http\Traits\ApiResponser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class AuthMutator
{
    use ApiResponser;

    public function register($rootValue, array $args, GraphQLContext $context)
    {
        $user = new User();
        $user->email = $args['email'];
        $user->password = bcrypt($args['password']);
        $user->name = $args['name'];
        $user->age = $args['age'];
        $user->address = $args['address'];
        $user->phone = $args['phone'];
        $user->save();
        $token =  $user->createToken('Advantures')->plainTextToken;
        return $this->success(['token' => $token, 'user' => $user], 'Registered Successfully');
    }

    public function login($rootValue, array $args, GraphQLContext $context)
    {
        if (Auth::attempt(['email' => $args['email'], 'password' => $args['password']])) {
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->plainTextToken;
            return $this->success(['token' => $token, 'user' => $user], 'Login Successfully');
        } else {
            return $this->error(null, 'Email or Password is Invalid.');
        }
    }

    public function logout($rootValue, array $args, GraphQLContext $context)
    {
        Auth::logout();
        return $this->success(null, 'Logout Successfully');
    }

    public function updateProfile($rootValue, array $args, GraphQLContext $context)
    {
        $user = User::findOrFail($context->user()->id);
        $user->name = $args['name'] ?? $user->name;
        $user->age = $args['age'] ?? $user->age;
        $user->address = $args['address'] ?? $user->address;
        $user->save();
        return $this->success($user, 'Updated Successfully');
    }
}
