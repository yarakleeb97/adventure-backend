<?php

namespace App\GraphQL\Mutations\User;

use App\Http\Traits\ApiResponser;
use App\Models\Subscription;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SubscriptionMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $subscription = new Subscription();
        $subscription->email = $args['email'];
        $subscription->save();
        return $this->success($subscription, 'Create Successfully');
    }
}
