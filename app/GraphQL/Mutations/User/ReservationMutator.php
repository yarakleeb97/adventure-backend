<?php

namespace App\GraphQL\Mutations\User;

use Stripe\Stripe;
use App\Http\Traits\ApiResponser;
use App\Models\Activity;
use App\Models\Reservation;
use Exception;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class ReservationMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $activity = Activity::findOrFail($args['activity_id']);
        $reservation = new Reservation();
        // $reservation->user_id = $context->user()->id;
        $reservation->user_id = 1;
        $reservation->activity_id = $args['activity_id'];
        $reservation->person_count = $args['person_count'];
        $reservation->type_payment = $args['type_payment'];
        $reservation->amount = $args['person_count'] * $activity->cost;
        $reservation->save();
        return $this->success($reservation, 'Reservation Successfully');
    }

    public function cancel($rootValue, array $args, GraphQLContext $context)
    {
        if ($reservation = Reservation::where('id', $args['id'])->where('user_id', $context->user()->id)->first()) {
            $reservation->coming = false;
            $reservation->save();
            return $this->success(null, 'Deleted Successfully');
        } else {
            return $this->error(null, 'Something went wrong');
        }
    }
}
