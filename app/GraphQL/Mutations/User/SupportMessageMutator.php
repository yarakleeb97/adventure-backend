<?php

namespace App\GraphQL\Mutations\User;

use App\Http\Traits\ApiResponser;
use App\Models\SupportMessage;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SupportMessageMutator
{
    use ApiResponser;

    public function create($rootValue, array $args, GraphQLContext $context)
    {
        $message = new SupportMessage();
        $message->name = $args['name'];
        $message->email = $args['email'];
        $message->subject = $args['subject'];
        $message->message = $args['message'];
        $message->save();
        return $this->success($message, 'Create Successfully');
    }
}
