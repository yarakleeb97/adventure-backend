<?php

namespace App\GraphQL;

class Constant
{
    const TYPE = ["CAMP", "EVENT"];
    const CAMP = "CAMP";
    const EVENT = "EVENT";
    const COUNT = 10;
    const PAGE = 1;
    const INTEREST = "interest";
    const REGION = "region_id";
    const ORGANISER = "organiser_id";
    const MONTH = "month";
    const FILTERS = ["age", "interest", "region_id","organiser_id", "start_date", "month"];
    const MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const AGE_LEVELS = ["Childs", "Teenagers", "Adults"];
}
