<?php

namespace App\GraphQL\Validators\Mutation;

use Nuwave\Lighthouse\Validation\Validator;

final class UploadFileValidator extends Validator
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:2048', 'mimes:jpg,bmp,png,jpeg']
        ];
    }

    public function messages(): array
    {
        return [
            'file.unique' => 'The file is required',
            'file.max' => 'The file size bigger than 2M',
            'file.mimes' => 'This type of file can not be upload',
        ];
    }
}
