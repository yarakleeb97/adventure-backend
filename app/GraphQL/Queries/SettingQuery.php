<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Setting;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SettingQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Setting::findOrFail($args['id']));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $count = Constant::COUNT;
        $page = Constant::PAGE;
        if (isset($args['page'])) {
            $page = $args['page'];
        }
        $settings = Setting::query();
        return  $this->success($settings->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $settings->paginate($count)->lastPage());
    }

    public function socialSettings($rootValue, array $args, GraphQLContext $context)
    {
        $settings = Setting::whereIn('key',['facebook','instagram','twitter','tiktok'])->get();
        return  $this->success($settings);
    }

    public function termsAndCondtionsSettings($rootValue, array $args, GraphQLContext $context)
    {
        $settings = Setting::where('key','termsAndCondtions')->first();
        return  $this->success($settings);
    }

    public function privacySettings($rootValue, array $args, GraphQLContext $context)
    {
        $settings = Setting::where('key','privacy')->first();
        return  $this->success($settings);
    }
}
