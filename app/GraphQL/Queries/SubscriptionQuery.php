<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Subscription;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SubscriptionQuery
{
    use ApiResponser;

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $count = Constant::COUNT;
        $page = Constant::PAGE;
        if (isset($args['page'])) {
            $page = $args['page'];
        }
        $subscriptions = Subscription::query();
        return  $this->success($subscriptions->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $subscriptions->paginate($count)->lastPage());
    }
}
