<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class UserQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(User::findOrFail($args['id']));
    }

    public function showProfile($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(User::findOrFail(Auth::user()->id));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $page = Constant::PAGE;
        $count = Constant::COUNT;
        $users = User::query();
        if (isset($args['page'])) {
            $page = ($args['page']);
        }
        return  $this->success($users->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $users->paginate($count)->lastPage());
    }
}
