<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Activity;
use App\Models\Interest;
use App\Models\Organiser;
use App\Models\Region;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class ActivityQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Activity::findOrFail($args['id']));
    }

    public function lastActivities($rootValue, array $args, GraphQLContext $context)
    {
        Organiser::findOrFail($args['organiser_id']);
        return  $this->success(Activity::where('organiser_id', $args['organiser_id'])->orderBy('created_at', 'desc')->take(6)->get());
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $page = Constant::PAGE;
        $count = Constant::COUNT;
        $activities = Activity::query();
        if (isset($args['type'])) {
            $activities = $activities->where('type', $args['type']);
        }
        if (isset($args['page'])) {
            $page = ($args['page']);
        }
        if (isset($args['filters'])) {
            if (!$activities = $this->ActivityFiltering($args['filters'], $activities)) {
                return  $this->error(null, 'Something Went Wrong Try Again');
            }
        }
        return  $this->success($activities->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $activities->paginate($count)->lastPage());
    }

    private function ActivityFiltering($filters, $activities)
    {
        for ($i = 0; $i < sizeof($filters); $i++) {
            $column = $filters[$i]['column'];
            $value = $filters[$i]['value'];
            if (in_array($column, Constant::FILTERS)) {
                if ($column == Constant::INTEREST) {
                    $interest =  Interest::where('name', $value)->firstOrFail();
                    $activities = $activities->whereHas('activityInterests', function ($query) use ($interest) {
                        $query->where('interest_id', $interest->id);
                    });
                } elseif ($column == Constant::REGION) {
                    $region =  Region::where('name', $value)->firstOrFail();
                    $activities = $activities->where($column, $region->id);
                } elseif ($column == Constant::ORGANISER) {
                    $organiser = Organiser::where('name', $value)->firstOrFail();
                    $activities = $activities->where($column, $organiser->id);
                } elseif ($column == Constant::MONTH) {
                    $activities = $activities->whereRaw('MONTH(start_date) = ?', [$value]);
                }
            } else {
                return  false;
            }
        }
        return $activities;
    }
}
