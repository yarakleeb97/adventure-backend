<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Region;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class RegionQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Region::findOrFail($args['id']));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $count = Constant::COUNT;
        $page = Constant::PAGE;
        if (isset($args['page'])) {
            $page = $args['page'];
        }
        $regions = Region::query();
        return  $this->success($regions->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $regions->paginate($count)->lastPage());
    }
}
