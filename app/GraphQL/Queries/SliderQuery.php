<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Slider;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SliderQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Slider::findOrFail($args['id']));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $count = Constant::COUNT;
        $page = Constant::PAGE;
        if (isset($args['page'])) {
            $page = $args['page'];
        }
        $sliders = Slider::query();
        return  $this->success($sliders->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $sliders->paginate($count)->lastPage());
    }
}
