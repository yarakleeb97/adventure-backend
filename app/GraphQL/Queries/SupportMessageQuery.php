<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\SupportMessage;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class SupportMessageQuery
{
    use ApiResponser;

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $count = Constant::COUNT;
        $page = Constant::PAGE;
        if (isset($args['page'])) {
            $page = $args['page'];
        }
        $messages = SupportMessage::query();
        return  $this->success($messages->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $messages->paginate($count)->lastPage());
    }
}
