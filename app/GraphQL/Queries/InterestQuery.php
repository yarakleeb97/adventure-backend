<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Interest; 
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class InterestQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Interest::findOrFail($args['id']));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $count = Constant::COUNT;
        $page = Constant::PAGE;
        if (isset($args['page'])) {
            $page = $args['page'];
        }
        $interest = Interest::query();
        return  $this->success($interest->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $interest->paginate($count)->lastPage());
    }
}
