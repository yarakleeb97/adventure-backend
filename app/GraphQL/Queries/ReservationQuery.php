<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Reservation;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class ReservationQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Reservation::findOrFail($args['id']));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $page = Constant::PAGE;
        $count = Constant::COUNT;
        $reservations = Reservation::query();
        if (isset($args['page'])) {
            $page = ($args['page']);
        }
        return  $this->success($reservations->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $reservations->paginate($count)->lastPage());
    }

    public function userReservations($rootValue, array $args, GraphQLContext $context)
    {
        $page = Constant::PAGE;
        $count = Constant::COUNT;
        $reservations = Reservation::where('user_id', $context->user()->id);
        if (isset($args['page'])) {
            $page = ($args['page']);
        }
        return  $this->success($reservations->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $reservations->paginate($count)->lastPage());
    }
}
