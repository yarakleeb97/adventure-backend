<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Constant;
use App\Http\Traits\ApiResponser;
use App\Models\Organiser;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class OrganiserQuery
{
    use ApiResponser;

    public function show($rootValue, array $args, GraphQLContext $context)
    {
        return  $this->success(Organiser::findOrFail($args['id']));
    }

    public function index($rootValue, array $args, GraphQLContext $context)
    {
        $page = Constant::PAGE;
        $count = Constant::COUNT;
        $organisers = Organiser::query();
        if (isset($args['page'])) {
            $page = ($args['page']);
        }
        return  $this->success($organisers->offset(($page - 1) *  $count)->limit($count)->get(), null, $page, $organisers->paginate($count)->lastPage());
    }
}
