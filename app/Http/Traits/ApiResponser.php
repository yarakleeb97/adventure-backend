<?php

namespace App\Http\Traits;

trait ApiResponser
{
    protected function success($data = null, string $message = null, int $current_page = null, int $last_page = null) {
        return [
            'result' => true,
            'message' => $message ?? "Done Successfully",
            'data' => $data,
            'current_page' => $current_page,
            'last_page' => $last_page
        ];
    }

    protected function error($data = null, string $message = null)
    {
        return [
            'result' => false,
            'message' => $message ?? "An error encountered while performing the operation",
            'data' => $data
        ];
    }
}
