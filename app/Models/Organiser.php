<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organiser extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['name', 'description', 'address', 'logo', 'email', 'phone'];

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($organiser) {
            $organiser->activities()->delete();
        });
        static::restoring(function ($organiser) {
            $organiser->activities()->onlyTrashed()->restore();
        });
    }
}
