<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['title','type','address', 'description', 'cover','region_id', 'organiser_id', 'start_date', 'end_date', 'age', 'cost'];

    public function organiser()
    {
        return $this->belongsTo(Organiser::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function interests()
    {
        return $this->belongsToMany(Interest::class);
    }

    public function activityInterests()
    {
        return $this->hasMany(ActivityInterest::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($activity) {
            $activity->activityInterests()->delete();
            $activity->reservations()->delete();
        });
        static::restoring(function ($activity) {
            $activity->activityInterests()->onlyTrashed()->restore();
            $activity->reservations()->onlyTrashed()->restore();
        });
    }
}
