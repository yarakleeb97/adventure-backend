<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityInterest extends Model
{
    use HasFactory;

    protected $table = "activity_interest";
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function interest()
    {
        return $this->belongsTo(Interest::class);
    }
}
