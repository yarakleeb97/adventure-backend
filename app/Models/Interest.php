<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['name'];

    public function activities()
    {
        return $this->belongsToMany(Activity::class);
    }

    public function activityInterest()
    {
        return $this->hasMany(ActivityInterest::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($interest) {
            $interest->activityInterest()->delete();
        });
        static::restoring(function ($interest) {
            $interest->activityInterest()->onlyTrashed()->restore();
        });
    }
}
