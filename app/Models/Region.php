<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['name'];

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($region) {
            $region->activities()->delete();
        });
        static::restoring(function ($region) {
            $region->activities()->onlyTrashed()->restore();
        });
    }
}
